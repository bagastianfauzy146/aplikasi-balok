package com.exampel.ifanbf.aplikasibalok;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class activity_result extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Intent intent = getIntent();

        String result = intent.getStringExtra("data");
        String hasil = intent.getStringExtra("data2");
        TextView tv = findViewById(R.id.tvResult);
        TextView tv2 = findViewById(R.id.hasil);
        tv.setText(result);
        tv2.setText(hasil);
    }
}

package com.exampel.ifanbf.aplikasibalok;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private EditText et_panjang, et_lebar, et_tinggi;
    private double p, l, t ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_panjang = findViewById(R.id.pnjng);
        et_lebar = findViewById(R.id.lbr);
        et_tinggi = findViewById(R.id.tgi);
    }

    private void data(List<String> variabel){
        String panjang = et_panjang.getText().toString();
        String lebar = et_lebar.getText().toString();
        String tinggi = et_tinggi.getText().toString();
        p = Double.parseDouble(panjang);
        l = Double.parseDouble(lebar);
        t = Double.parseDouble(tinggi);
    }
    public void volume(View view) {
        if (et_panjang.getText().toString().isEmpty() && et_lebar.getText().toString().isEmpty() && et_tinggi.getText().toString().isEmpty()){
            et_panjang.setError("harap diisi dahulu");
            et_lebar.setError("harap diisi dahulu");
            et_tinggi.setError("harap diisi dahulu");
            return;
        }
        if (et_panjang.getText().toString().isEmpty() && et_lebar.getText().toString().isEmpty()){
            et_panjang.setError("harap diisi dahulu");
            et_lebar.setError("harap diisi dahulu");
            return;
        }
        if (et_panjang.getText().toString().isEmpty() && et_tinggi.getText().toString().isEmpty()){
            et_panjang.setError("harap diisi dahulu");
            et_tinggi.setError("harap diisi dahulu");
            return;
        }
        if (et_lebar.getText().toString().isEmpty() && et_tinggi.getText().toString().isEmpty()){
            et_lebar.setError("harap diisi dahulu");
            et_tinggi.setError("harap diisi dahulu");
            return;
        }
        if (et_panjang.getText().toString().isEmpty()){
            et_panjang.setError("harap diisi dahulu");
            return;
        }
        if (et_tinggi.getText().toString().isEmpty()){
            et_tinggi.setError("harap diisi dahulu");
            return;
        }
        if (et_lebar.getText().toString().isEmpty()){
            et_lebar.setError("harap diisi dahulu");
            return;
        }

        List<String> variabel = new ArrayList<>();
        data(variabel);

        Double volume = p*l*t;
        String nama = "Volume";

        Intent intent = new Intent(this, activity_result.class);
        intent.putExtra("data2", String.valueOf(nama));
        intent.putExtra("data", String.valueOf(volume));

        startActivity(intent);

    }

    public void luas(View view) {
        if (et_panjang.getText().toString().isEmpty() && et_lebar.getText().toString().isEmpty() && et_tinggi.getText().toString().isEmpty()){
            et_panjang.setError("harap diisi dahulu");
            et_lebar.setError("harap diisi dahulu");
            et_tinggi.setError("harap diisi dahulu");
            return;
        }
        if (et_panjang.getText().toString().isEmpty() && et_lebar.getText().toString().isEmpty()){
            et_panjang.setError("harap diisi dahulu");
            et_lebar.setError("harap diisi dahulu");
            return;
        }
        if (et_panjang.getText().toString().isEmpty() && et_tinggi.getText().toString().isEmpty()){
            et_panjang.setError("harap diisi dahulu");
            et_tinggi.setError("harap diisi dahulu");
            return;
        }
        if (et_lebar.getText().toString().isEmpty() && et_tinggi.getText().toString().isEmpty()){
            et_lebar.setError("harap diisi dahulu");
            et_tinggi.setError("harap diisi dahulu");
            return;
        }
        if (et_panjang.getText().toString().isEmpty()){
            et_panjang.setError("harap diisi dahulu");
            return;
        }
        if (et_tinggi.getText().toString().isEmpty()){
            et_tinggi.setError("harap diisi dahulu");
            return;
        }
        if (et_lebar.getText().toString().isEmpty()){
            et_lebar.setError("harap diisi dahulu");
            return;
        }

        List<String> variabel = new ArrayList<>();
        data(variabel);

        Double luas = 2* (p*l+p*t+l*t);
        String nama = "Luas";

        Intent intent = new Intent(this, activity_result.class);
        intent.putExtra("data", String.valueOf(luas));
        intent.putExtra("data2", String.valueOf(nama));

        startActivity(intent);

    }

    public void keliling(View view) {
        if (et_panjang.getText().toString().isEmpty() && et_lebar.getText().toString().isEmpty() && et_tinggi.getText().toString().isEmpty()){
            et_panjang.setError("harap diisi dahulu");
            et_lebar.setError("harap diisi dahulu");
            et_tinggi.setError("harap diisi dahulu");
            return;
        }
        if (et_panjang.getText().toString().isEmpty() && et_lebar.getText().toString().isEmpty()){
            et_panjang.setError("harap diisi dahulu");
            et_lebar.setError("harap diisi dahulu");
            return;
        }
        if (et_panjang.getText().toString().isEmpty() && et_tinggi.getText().toString().isEmpty()){
            et_panjang.setError("harap diisi dahulu");
            et_tinggi.setError("harap diisi dahulu");
            return;
        }
        if (et_lebar.getText().toString().isEmpty() && et_tinggi.getText().toString().isEmpty()){
            et_lebar.setError("harap diisi dahulu");
            et_tinggi.setError("harap diisi dahulu");
            return;
        }
        if (et_panjang.getText().toString().isEmpty()){
            et_panjang.setError("harap diisi dahulu");
            return;
        }
        if (et_tinggi.getText().toString().isEmpty()){
            et_tinggi.setError("harap diisi dahulu");
            return;
        }
        if (et_lebar.getText().toString().isEmpty()){
            et_lebar.setError("harap diisi dahulu");
            return;
        }

        List<String> variabel = new ArrayList<>();
        data(variabel);

        Double keliling = p*4 + l*4 + t*4;
        String nama = "keliling";

        Intent intent = new Intent(this, activity_result.class);
        intent.putExtra("data", String.valueOf(keliling));
        intent.putExtra("data2", String.valueOf(nama));

        startActivity(intent);
    }
}
